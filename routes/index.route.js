const fs = require('fs');
let textModel = 'Hello from my Node.js!';

const generateHtmlForm = () => {
  return `
  <html lang="eng">
    <title>Some form</title>
    <body>
      <form action="/message" method="POST"><input type="text" name="message" placeholder="Enter message"/></form>
      <h2>${textModel}</h2>
    </body>
  </html>`;
};

const htmlResponse = `
<html lang="eng">
  <title>Some title</title>
  <body>
    <h2>${textModel}</h2>
  </body>
</html>`;

module.exports = (req, res) => {
  textModel = fs.readFileSync('./hello.txt', {encoding: "utf8"});
  res.send(generateHtmlForm());
}