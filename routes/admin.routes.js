const express = require('express');
const Router = express.Router();

// Controllers
const adminController = require('../controllers/admin.controller');

Router.get('/add-product', adminController.getAddProduct)
Router.get('/products', adminController.getAdminProducts)
Router.post('/products', adminController.createProduct)
Router.get('/products/delete', adminController.deleteProduct)
Router.get('/products/:id', adminController.getAdminProduct)
Router.post('/products/:id', adminController.updateProduct)

module.exports = Router;