const fs = require('fs');

module.exports = (req, res) => {
  const data = req.body;
  fs.writeFile('hello.txt', data.message, (err) => {  });

  res.redirect('/')
}

// module.exports = (req, res) => {
//   const body = [];
//   req.on('data', chunk => {
//     body.push(chunk);
//   });
//
//   req.on('end', () => {
//     const parsedBody = Buffer.concat(body).toString();
//     const bodyParams = new URLSearchParams(parsedBody);
//     fs.writeFile('hello.txt', bodyParams.get('message'), (err) => {  });
//   });
//
//   res.writeHead(302, {
//     'Location': '/'
//   });
//   return res.end();
// }