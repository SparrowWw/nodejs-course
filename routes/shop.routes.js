const express = require('express');
const Router = express.Router();

// Controller
const shopController = require('../controllers/shop.controller');

// home page
Router.get('/', shopController.getHomePage)
Router.get('/products', shopController.getProductsPage)
Router.get('/products/:id', shopController.getProductDetails)
Router.get('/cart', shopController.getCartPage)
Router.post('/cart', shopController.addCartProduct)
Router.get('/cart/delete', shopController.deleteCartProduct)
Router.get('/orders', shopController.getOrdersPage)
Router.post('/create-order', shopController.postOrder)

// any route match
Router.use(shopController.get404Page)

module.exports = Router;