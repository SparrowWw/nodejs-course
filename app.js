const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()
const adminRoutes = require('./routes/admin.routes')
const shopRoutes = require('./routes/shop.routes')
const shopController = require('./controllers/shop.controller')
const sequelize = require('./helpers/database')
const Product = require('./models/products')
const Cart = require('./models/cart')
const CartItem = require('./models/cart-item')
const User = require('./models/user')
const Order = require('./models/order')
const OrderItem = require('./models/order-item')

// config
app.set('view engine', 'pug')
app.locals.basedir = app.get('views')

// body-parser middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

// helpers
app.use(async (req, res, next) => { req.user = await User.findByPk(1); next() })
app.use(shopController.setCartQty)

// routes
app.use('/admin', adminRoutes)
app.use(shopRoutes)

// db relations
Product.belongsTo(User)
User.hasMany(Product)
User.hasOne(Cart)
Cart.belongsToMany(Product, { through: CartItem })
// Product.belongsToMany(Cart, { through: CartItem })
Order.belongsTo(User)
User.hasMany(Order)
Order.belongsToMany(Product, { through: OrderItem })

sequelize
    // .sync({ force: true })
    .sync()
    .then(() => User.findOrCreate({ where: { name: 'Andrew', email: 'voolve@gmail.com' }}))
    .then(async ([ user ]) => {
        const cart = await user.getCart()
        if (!cart) {
            return user.createCart()
        }
    })
    .then(() => app.listen(3001))
