const Product = require('../models/products.js');

exports.getAdminProducts = async (req, res) => {
  const products = await req.user.getProducts()
  res.render('admin/products', { products, docTitle: 'Admin Product', route: req.originalUrl, isAdmin: true })
}

exports.getAddProduct = (req, res) => {
  res.render('admin/edit-product', { product: {}, docTitle: 'Add Product', route: req.originalUrl })
}

exports.getAdminProduct = async (req, res, next) => {
  const product = await Product.findByPk(req.params.id)
  if (product) {
    res.render('admin/edit-product', { product, docTitle: 'Edit Product', route: '/admin/products' })
  } else {
    res.redirect('/admin/products')
  }
}

exports.createProduct = async (req, res, next) => {
  req.user.createProduct({ ...req.body }).then().catch().finally(() => {
    res.redirect('/')
  })
}

exports.updateProduct = (req, res, next) => {
  Product.update({ ...req.body }, { where: { id: Number(req.params.id) }})
    .catch(console.log)
    .finally(() => {
      res.redirect('/admin/products');
    })
}

exports.deleteProduct = (req, res, next) => {
  Product.destroy({ where: { id: Number(req.query.id) }})
  res.redirect('/admin/products');
}