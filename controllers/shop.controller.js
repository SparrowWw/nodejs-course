const rootDir = require('../helpers/path');
const path = require('path');
const Product = require('../models/products.js');
const Cart = require('../models/cart.js');
const Order = require('../models/cart.js');

// general middleware for cart qty in header
exports.setCartQty = async (req, res, next) => {
  const cart = await req.user.getCart()
  res.locals.cartQty = await cart.countProducts()
  next()
}

exports.getHomePage = async (req, res) => {
  // res.sendFile(path.join(rootDir, 'views', 'shop.html'))
  const products = await Product.findAll()
  res.render('shop/index', { products, docTitle: 'Main Shop', route: req.url })
}

exports.getProductsPage = async (req, res) => {
  const products = await Product.findAll()
  res.render('shop/product-list', { products, docTitle: 'All Products List', route: req.url })
}

exports.getProductDetails = async (req, res)  => {
  const product = await Product.findByPk(req.params.id)
  
  if (product) {
    res.render('shop/product-details', { product, docTitle: `Product - ${product.title}`, route: '/products' })
  } else {
    res.redirect('/')
  }
}

exports.getCartPage = async (req, res) => {
  const fetchCart = await req.user.getCart()
  const products = await fetchCart.getProducts()
  const totalPrice = products.reduce((prev, current) => {
    return prev + (current.price * current.cartItem.quantity)
  }, 0)

  res.render('shop/cart', { cart: { products, totalPrice }, docTitle: 'Cart', route: req.url })
}

exports.addCartProduct = async (req, res) => {
  const fetchCart = await req.user.getCart()
  const products = await fetchCart.getProducts({ where: { id: req.body.productId }})
  let product = products[0]
  let newQty = 1

  if (product) newQty = product.cartItem.quantity + 1
  product = await Product.findByPk(req.body.productId)

  await fetchCart.addProduct(product, {
    through: { quantity: newQty }
  })

  res.redirect('/cart')
}

exports.deleteCartProduct = async (req, res) => {
  const fetchCart = await req.user.getCart()
  // console.log(Object.keys(fetchCart.__proto__))
  fetchCart.removeProduct(req.query.id).finally(() => {
    res.redirect('/cart')
  })
}

exports.getOrdersPage = async (req, res) => {
  const orders = await req.user.getOrders({ include: ['products'] })
  res.render('shop/orders', { orders, docTitle: 'My Orders', route: req.url })
}

exports.postOrder = async (req, res) => {
  const fetchCart = await req.user.getCart()
  const order = await req.user.createOrder()
  const products = await fetchCart.getProducts().map(product => {
    product.orderItem = { quantity: product.cartItem.quantity }
    return product
  })
  order.addProducts(products)
    .then(() => {
      fetchCart.setProducts(null)
    })
    .finally(() => {
      res.redirect('/orders')
    })
}

exports.get404Page = (req, res) => {
  res.status(404).sendFile(path.join(rootDir, 'views', 'error.html'))
}