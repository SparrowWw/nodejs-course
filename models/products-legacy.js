const rootDir = require('../helpers/path');
const fs = require('fs');
const path = require('path');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const db = require('../helpers/database')

const p = path.join(rootDir, 'models', 'data.json');

class Product {
  constructor({ title, imageUrl, description, price, id }) {
    this.title = title || 'Default title'
    this.imageUrl = imageUrl || 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
    this.description = description || 'A very interesting book'
    this.price = price
    this.id = id
  }

  async save () {
    return db.execute(
      'INSERT INTO products (title, price, imageUrl, description) VALUES (?, ?, ?, ?)',
      [this.title, this.price, this.imageUrl, this.description]
    ).then().catch(console.log)

    // const db = await this.constructor.fetchDb()
    // db.products.push(this)
    // this.constructor.writeFile(db)
    // return this
  }

  async update () {
    return db.execute(
      'UPDATE products SET title = ?, price = ?, imageUrl = ?, description = ? WHERE id = ?',
      [this.title, this.price, this.imageUrl, this.description, this.id]
    ).then().catch(console.log)

    // const db = await this.constructor.fetchDb()
    // const productIndex = db.products.findIndex(p => p.id === this.id)
    // db.products[productIndex] = this

    // this.constructor.writeFile(db)
    // return this
  }

  static writeFile (elements) {
    fs.writeFile(p, JSON.stringify({ ...elements }), (err) => {  })
  }

  static async fetchProductById (id) {
    return db.execute('SELECT * FROM products WHERE id = ?', [Number(id)]).then(([ rows ]) => {
      return rows[0]
    }).catch(console.log)
  }

  static fetchProducts () {
    return db.execute('SELECT * FROM products').then(([ rows ]) => rows).catch(console.log)
    // return readFile(p).then(json => JSON.parse(json).products)
  }

  static async deleteProduct (id) {
    return Promise.all([
      db.execute('DELETE FROM products WHERE id = ?', [Number(id)])
    ])
    // const db = await this.fetchDb()
    // const productIndex = db.products.findIndex(p => p.id === Number(id))
    // if (productIndex >= 0) db.products.splice(productIndex, 1)
    
    // // remove from cart also
    // const cartItemIndex = db.cart.findIndex(item => item.id === Number(id))
    // if (cartItemIndex >= 0) db.cart.splice(cartItemIndex, 1)
    // this.writeFile(db)
  }

  static async saveToCart (product) {
    const db = await this.fetchDb()
    const itemExistInIndex = db.cart.findIndex(item => item.id === product.id)

    if (itemExistInIndex === -1) {
      db.cart.push({ id: product.id, qty: 1 })
    } else {
      db.cart[itemExistInIndex] = { id: product.id, qty: db.cart[itemExistInIndex].qty + 1 }
    }
    this.writeFile(db)
  }

  static async removeFromCart (id) {
    const db = await this.fetchDb()
    const productIndex = db.cart.findIndex(item => item.id === Number(id))
    if (productIndex >= 0) db.cart.splice(productIndex, 1)

    this.writeFile(db)
  }

  static fetchCart () {
    return readFile(p).then(json => {
      const db = JSON.parse(json);
      // populate cart items with products data
      const products = db.cart.map(item => ({ ...db.products.find(p => p.id === item.id), qty: item.qty }));
      const totalPrice = products.reduce((prev, current) => {
        return prev += current.price * current.qty
      }, 0)

      return { products, totalPrice }
    })
  }

  static fetchDb () {
    return readFile(p).then(json => JSON.parse(json) || {})
  }
}

module.exports = Product